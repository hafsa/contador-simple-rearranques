#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Simple HTTP Server
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# SARO and SAT subjects (Universidad Rey Juan Carlos)
# January 2019
#
# Important: Run with Python 3.6 or higher
#
# Session server 1.
# Serves a form, showing in the same page the last content of the form.
# This is a basic version, using GET for sending the contents of the form,
# and a cookie with the last content of the form.

import http.server
import http.cookies

contador = 5
class Handler(http.server.BaseHTTPRequestHandler):

    def actualizar_valor(self):
        global contador
        contador_actual = contador
        contador -= 1
        if contador < 0:
            contador = 5
        return contador_actual

    def enviar_respuesta(self, contador_actual):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(bytes("El n&uacute;mero inverso es: " + str(contador_actual), 'utf8'))

    def enviar_respuesta_ERROR(self):
        self.send_response(404)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(bytes("Error 404 - P&aacute;gina no encontrada", 'utf-8'))

    def do_GET(self):
        received = self.path[1:]
        if received == "contador-inverso":
            contador_actual = self.actualizar_valor()
            self.enviar_respuesta(contador_actual)
        else:
            self.enviar_respuesta_ERROR()

if __name__ == "__main__":
    servidor = http.server.HTTPServer(('localhost', 1234), Handler)
    print('Servidor iniciado en http://localhost:1234')
    print("Esperando a que algún usuario introduzca la url del contador inverso")
    servidor.serve_forever()